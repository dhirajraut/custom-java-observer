package com.sparkvio.designpatterns.observer.listeners;

import com.sparkvio.designpatterns.observer.observable.IOrderListener;
import com.sparkvio.designpatterns.observer.observable.Order;
import com.sparkvio.designpatterns.observer.observable.OrderState;
import com.sparkvio.designpatterns.observer.operations.OrderPreparationThread;

public class Restaurant implements IOrderListener {

	
	public void onOrderStateChanged (Order order) {
		if (order.getOrderState() == OrderState.ORDER_INITIATED) {
			
			order.setOrderState(OrderState.ACCEPTED_BY_RESTAURANT);
			prepareFood(order);
		}		
	}
	
	public void prepareFood(Order order) {
		
		OrderPreparationThread orderPreparationThread = new OrderPreparationThread(order);
		Thread thread = new Thread(orderPreparationThread);
		thread.start();
	}
}
