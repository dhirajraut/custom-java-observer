package com.sparkvio.designpatterns.observer.listeners;

import com.sparkvio.designpatterns.observer.observable.IOrderListener;
import com.sparkvio.designpatterns.observer.observable.Order;

public class Client implements IOrderListener {

	public void initiateOrder() {
    	Order order = new Order(/* Customer */ this, "Chicken Lolipop", 1, new Restaurant());
	}
	
	public void onOrderStateChanged(Order order) {
		System.out.println("Order State Changed: " + order.getOrderState());
	}
}
