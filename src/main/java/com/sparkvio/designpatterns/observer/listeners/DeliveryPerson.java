package com.sparkvio.designpatterns.observer.listeners;

import com.sparkvio.designpatterns.observer.observable.IOrderListener;
import com.sparkvio.designpatterns.observer.observable.Order;
import com.sparkvio.designpatterns.observer.observable.OrderState;
import com.sparkvio.designpatterns.observer.operations.DeliveryThread;

public class DeliveryPerson implements IOrderListener {

	public void onOrderStateChanged(Order order) {
		if (order.getOrderState() == OrderState.READY_FOR_DELIVERY) {

			order.setOrderState(OrderState.ORDER_PICKED_UP);
			deliverOrder(order);
		}
	}
	
	public void deliverOrder(Order order) {
		
		DeliveryThread deliveryThread = new DeliveryThread (order);
		Thread thread = new Thread(deliveryThread);
		thread.start();
	}
}
