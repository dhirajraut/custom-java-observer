package com.sparkvio.designpatterns.observer.operations;

import com.sparkvio.designpatterns.observer.observable.Order;
import com.sparkvio.designpatterns.observer.observable.OrderState;

public class DeliveryThread implements Runnable {
	
	Order order;
	public DeliveryThread(Order order) {
		this.order = order;
	}
	public void run() {
		
		order.setOrderState(OrderState.ON_THE_WAY);

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		order.setOrderState(OrderState.DELIVERED);
	}
}