package com.sparkvio.designpatterns.observer.operations;

import com.sparkvio.designpatterns.observer.observable.Order;
import com.sparkvio.designpatterns.observer.observable.OrderState;

public class OrderPreparationThread implements Runnable {
	
	Order order;
	public OrderPreparationThread(Order order) {
		this.order = order;
	}
	public void run() {
		order.setOrderState(OrderState.PREPARING);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		order.setOrderState(OrderState.READY_FOR_DELIVERY);
	}
}