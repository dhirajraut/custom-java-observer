package com.sparkvio.designpatterns.observer;

import com.sparkvio.designpatterns.observer.listeners.Client;
public class App 
{
    public static void main( String[] args ) throws Exception {
    	Client client = new Client();
    	client.initiateOrder();
    }
}
