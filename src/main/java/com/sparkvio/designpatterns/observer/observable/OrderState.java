package com.sparkvio.designpatterns.observer.observable;

public enum OrderState {

	ORDER_INITIATED,
	ACCEPTED_BY_RESTAURANT,
	PREPARING,
	READY_FOR_DELIVERY,
	ORDER_PICKED_UP,
	ON_THE_WAY,
	DELIVERED
}
