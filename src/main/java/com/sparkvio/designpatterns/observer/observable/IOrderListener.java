package com.sparkvio.designpatterns.observer.observable;

public interface IOrderListener {

	public void onOrderStateChanged(Order order);
}
