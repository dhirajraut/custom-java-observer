package com.sparkvio.designpatterns.observer.observable;

import java.util.ArrayList;
import java.util.List;

import com.sparkvio.designpatterns.observer.listeners.Client;
import com.sparkvio.designpatterns.observer.listeners.DeliveryPerson;
import com.sparkvio.designpatterns.observer.listeners.Restaurant;


public class Order {

	private int orderId;
	private String itemName;
	private int itemQuantity;
	private OrderState orderState;
	
	public Order(Client client, String itemName, int itemQuantity, Restaurant restaurant) {
		this.orderId = 1; // Generate
		this.itemName = itemName;
		this.itemQuantity = itemQuantity;
		addListener(client);
		addListener(restaurant);
		addListener(new DeliveryPerson());
		
		setOrderState(OrderState.ORDER_INITIATED);
	}
	
	public OrderState getOrderState() {
		return orderState;
	}

	public void setOrderState(OrderState orderState) {
		this.orderState = orderState;
		notifyListeners();
	}

	List<IOrderListener> listeners = new ArrayList<IOrderListener>();
	public void addListener(IOrderListener listener) {
		
		listeners.add(listener);
	}
	
	public void removeListener(IOrderListener listener) {
		
		listeners.remove(listener);
	}

	public void notifyListeners() {
		
		for (IOrderListener orderListener : listeners) {
			orderListener.onOrderStateChanged(this);
		}
	}

}
